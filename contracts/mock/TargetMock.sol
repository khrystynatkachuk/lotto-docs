// SPDX-License-Identifier: MIT

pragma solidity 0.8.9;

contract TargetMock{
    string public helloString;
    constructor(){
        helloString = "Hello from constructor";
    }

    function setHello(string memory _hello) public{
        helloString = _hello;
    }
}