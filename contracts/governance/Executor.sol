// SPDX-License-Identifier: MIT
pragma solidity 0.8.9;
pragma abicoder v2;

import "./ProposalValidator.sol";
import "../interfaces/ILotteryGame.sol";
import "./interfaces/IGovernance.sol";
import "./interfaces/IExecutor.sol";
import "./LotteryGameMock.sol";
import "hardhat/console.sol";

/**
 * @title Executor and Validator Contract
 * @dev Contract:

 *- Validate Proposal creations/ cancellation
  - Validate Vote Quorum and Vote success on proposal
  - Queue, Execute, Cancel, successful proposals' transactions.
 **/
contract Executor is ProposalValidator, IExecutor {

    uint256 public executionPeriod;
    uint256 public minimumDelay;
    uint256 public maximumDelay;

    address private _admin;
    address private _pendingAdmin;
    uint256 private _delay;

    mapping(bytes32 => bool) private _queuedTransactions;

    /**
     * @dev Constructor
     * @param admin admin address, that can call the main functions, (should be Governance contract)
     * @param delay minimum time between queueing and execution of proposal
     * @param _executionPeriod time after `delay` while a proposal can be executed
     * @param _minimumDelay lower threshold of `delay`, in seconds
     * @param _maximumDelay upper threhold of `delay`, in seconds
     * @param propositionThreshold minimum percentage of supply needed to submit a proposal
     * @param voteDuration duration in seconds of the voting period
     * @param voteDifferential percentage of supply that `for` votes need to be over `against`
     *   in order for the proposal to pass
     * @param minimumQuorum minimum percentage of the supply in FOR-voting-power need for a proposal to pass
     **/
    constructor(
        address admin,
        uint256 delay,
        uint256 _executionPeriod,
        uint256 _minimumDelay,
        uint256 _maximumDelay,
        uint256 propositionThreshold,
        uint256 voteDuration,
        uint256 voteDifferential,
        uint256 minimumQuorum
    )
        ProposalValidator(
            propositionThreshold,
            voteDuration,
            voteDifferential,
            minimumQuorum
        )
    {
        require(delay >= _minimumDelay, "DELAY_SHORTER_THAN_MINIMUM");
        require(delay <= _maximumDelay, "DELAY_LONGER_THAN_MAXIMUM");
        _delay = delay;
        _admin = admin;

        executionPeriod = _executionPeriod;
        minimumDelay = _minimumDelay;
        maximumDelay = _maximumDelay;

        emit NewDelay(delay);
        emit NewAdmin(admin);
    }

    modifier onlyAdmin() {
        require(msg.sender == _admin, "ONLY_BY_ADMIN");
        _;
    }

    modifier onlyExecutor() {
        require(msg.sender == address(this), "ONLY_BY_THIS_TIMELOCK");
        _;
    }

    modifier onlyPendingAdmin() {
        require(msg.sender == _pendingAdmin, "ONLY_BY_PENDING_ADMIN");
        _;
    }

    /**
     * @dev Set the delay
     * @param delay delay between queue and execution of proposal
     **/
    function setDelay(uint256 delay) public onlyExecutor {
        _validateDelay(delay);
        _delay = delay;

        emit NewDelay(delay);
    }

    /**
     * @dev Function enabling pending admin to become admin
     * Can only be called by pending admin
     **/
    function acceptAdmin() public onlyPendingAdmin {
        _admin = msg.sender;
        _pendingAdmin = address(0);

        emit NewAdmin(msg.sender);
    }

    /**
     * @dev Setting a new pending admin (that can then become admin)
     * Can only be called by this executor (i.e via proposal)
     * @param newPendingAdmin address of the new admin
     **/
    function setPendingAdmin(address newPendingAdmin) public onlyExecutor {
        _pendingAdmin = newPendingAdmin;

        emit NewPendingAdmin(newPendingAdmin);
    }


    /**
     * @dev Set minimumQuorum
     * @notice can be changed only via proposal
     * @param _minimumQuorum percentage of min quorum need to proposal passed
     **/
    function setMinimumQuorum(uint256 _minimumQuorum) public onlyExecutor {
        minimumQuorum = _minimumQuorum;
    }

     /**
     * @dev Set voteDifferential
     * @notice can be changed only via proposal
     * @param _voteDifferential percentage of differential of votes between for and against votes
     **/
    function setVoteDifferential(uint256 _voteDifferential) public onlyExecutor {
        voteDifferential = _voteDifferential;
    }

     /**
     * @dev Set votingDuration
     * @notice can be changed only via proposal
     * @param _votingDuration time in seconds of voting period
     **/
    function setVotingDuration(uint256 _votingDuration) public onlyExecutor {
        votingDuration = _votingDuration;
    }

    /**
     * @dev Function, called by Governance, that queue a transaction, returns action hash
     * @param target smart contract target
     * @param value wei value of the transaction
     * @param signature function signature of the transaction
     * @param data function arguments of the transaction or callData if signature empty
     * @param executionTime time at which to execute the transaction
     * @return the action Hash
     **/
    function queueTransaction(
        address target,
        uint256 value,
        string memory signature,
        bytes memory data,
        uint256 executionTime
    ) public override onlyAdmin returns (bytes32) {
        require(
            executionTime >= block.timestamp + _delay,
            "EXECUTION_TIME_UNDERESTIMATED"
        );

        bytes32 actionHash = keccak256(
            abi.encode(target, value, signature, data, executionTime)
        );
        _queuedTransactions[actionHash] = true;

        emit QueuedAction(
            actionHash,
            target,
            value,
            signature,
            data,
            executionTime
        );
        return actionHash;
    }

    /**
     * @dev Function, called by Governance, that cancels a transaction, returns action hash
     * @param target smart contract target
     * @param value wei value of the transaction
     * @param signature function signature of the transaction
     * @param data function arguments of the transaction or callData if signature empty
     * @param executionTime time at which to execute the transaction
     * @return the action Hash of the canceled tx
     **/
    function cancelTransaction(
        address target,
        uint256 value,
        string memory signature,
        bytes memory data,
        uint256 executionTime
    ) public override onlyAdmin returns (bytes32) {
        bytes32 actionHash = keccak256(
            abi.encode(target, value, signature, data, executionTime)
        );
        _queuedTransactions[actionHash] = false;

        emit CancelledAction(
            actionHash,
            target,
            value,
            signature,
            data,
            executionTime
        );
        return actionHash;
    }

    /**
     * @dev Function, called by Governance, that cancels a transaction, returns the callData executed
     * @param target smart contract target
     * @param value wei value of the transaction
     * @param signature function signature of the transaction
     * @param data function arguments of the transaction or callData if signature empty
     * @param executionTime time at which to execute the transaction
     * @return the callData executed as memory bytes
     **/
    function executeTransaction(
        address target,
        uint256 value,
        string memory signature,
        bytes memory data,
        uint256 executionTime
    ) public payable override onlyAdmin returns (bytes memory) {
        bytes32 actionHash = keccak256(
            abi.encode(target, value, signature, data, executionTime)
        );
        require(_queuedTransactions[actionHash], "ACTION_NOT_QUEUED");
        require(block.timestamp >= executionTime, "TIMELOCK_NOT_FINISHED");
        require(
            block.timestamp <= executionTime + executionPeriod,
            "EXECUTION_PERIOD_FINISHED"
        );

        _queuedTransactions[actionHash] = false;

        bytes memory callData;
        // string temp = signature;

        if (bytes(signature).length == 0) {
            callData = data;
        } else {
            // callData = abi.encodePacked(bytes4(keccak256(("setHello(string memory _hello)"))), data);
            // TODO: check in tests
               callData = abi.encodePacked(bytes4(keccak256(bytes(signature))), data);
        }

        bool success;
        bytes memory resultData;
        // solhint-disable-next-line avoid-low-level-calls
        (success, resultData) = target.call{value: value}(callData);
        require(success, "FAILED_ACTION_EXECUTION");

        emit ExecutedAction(
            actionHash,
            target,
            value,
            signature,
            data,
            executionTime,
            resultData
        );

        return resultData;
    }

    /**
     * @dev Getter of the current admin address (should be governance)
     * @return The address of the current admin
     **/
    function getAdmin() external view override returns (address) {
        return _admin;
    }

    /**
     * @dev Getter of the current pending admin address
     * @return The address of the pending admin
     **/
    function getPendingAdmin() external view override returns (address) {
        return _pendingAdmin;
    }

    /**
     * @dev Getter of the delay between queuing and execution
     * @return The delay in seconds
     **/
    function getDelay() external view override returns (uint256) {
        return _delay;
    }

    /**
     * @dev Returns whether an action (via actionHash) is queued
     * @param actionHash hash of the action to be checked
     * keccak256(abi.encode(target, value, signature, data, executionTime))
     * @return true if underlying action of actionHash is queued
     **/
    function isActionQueued(bytes32 actionHash)
        external
        view
        override
        returns (bool)
    {
        return _queuedTransactions[actionHash];
    }

    /**
     * @dev Checks whether a proposal is over its execution period
     * @param governance Governance contract
     * @param proposalId Id of the proposal against which to test
     * @return true of proposal is over execution period
     **/
    function isProposalOverExecutionPeriod(
        IGovernance governance,
        uint256 proposalId
    ) external view override returns (bool) {
        IGovernance.ProposalWithoutVotes memory proposal = governance
            .getProposalById(proposalId);

        return (block.timestamp > proposal.executionTime + executionPeriod);
    }

    function _validateDelay(uint256 delay) internal view {
        require(delay >= minimumDelay, "DELAY_SHORTER_THAN_MINIMUM");
        require(delay <= maximumDelay, "DELAY_LONGER_THAN_MAXIMUM");
    }

    // solhint-disable-next-line no-empty-blocks
    receive() external payable {}
}
