// SPDX-License-Identifier: MIT
pragma solidity 0.8.9;
pragma abicoder v2;

import "./interfaces/IGovernanceStrategy.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract LotteryGameMock is Ownable {
    string public name;
    uint256 public participationFee;
    uint256 public callingRewad;

    // owner - Executor contract
    constructor(
        string memory _name,
        uint256 fee,
        uint256 reward
    ) Ownable() {
        name = _name;
        participationFee = fee;
        callingRewad = reward;
    }
}
