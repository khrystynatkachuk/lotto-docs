// SPDX-License-Identifier: MIT

pragma solidity 0.8.9;
pragma abicoder v2;

import "./interfaces/IGovernanceStrategy.sol";
import "./interfaces/IExecutor.sol";
import "./interfaces/IProposalValidator.sol";
import "./interfaces/IGovernance.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";


/**
 * @title Governance contract
 * @dev Main point of interaction with Lotto governance

 *- Create a Proposal
  - Cancel a Proposal
  - Queue a Proposal
  - Execute a Proposal
  - Submit Vote to a Proposal
  Available Proposal States : Pending => Active => Succeeded(/Failed) => Queued => Executed(/Expired).
  The transition  "Canceled" can appear while the State is not set as Executed/Expired
  
 *1. When the Proposal is created it has Pending state
  2. After is the time exeed the votingDelay value Proposal become Active so available for voting
  3. When the voting period is finished Proposal become or Succeeded or Failed. It depends on if the voteDifference was achived or not
  4. If the proposal passed it should be Queued and give Queued state
  5. After the proposal was queued there is delay beetween queue and execute. After the delay was expired transaction can be executed and give the state Executed
  6. If the executionPeriod was passed and transaction was not executed it recieve Expired state and can't be executed anymore
*/
contract Governance is Ownable, IGovernance {
    using SafeMath for uint256;

    address private _governanceStrategy;
    uint256 private _votingDelay;

    uint256 private _proposalsCount;
    mapping(uint256 => Proposal) private _proposals;
    mapping(address => bool) private _authorizedExecutors;

    address private _guardian;

    modifier onlyGuardian() {
        require(
            msg.sender == _guardian,
            "Governance: caller is not the quardian"
        );
        _;
    }


/**
   * @dev Constructor - set initial parameters
   * @param governanceStrategy - The whitelisted strategy to calculate voting power
   * @param votingDelay - delay before voting will be unlocked and proposal state changed from Pending ro Active
   * @param guardian - the address of guardian who will be able to cancel proposal
   * @param executors - list of executors
   **/
    constructor(
        address governanceStrategy,
        uint256 votingDelay,
        address guardian,
        address[] memory executors
    ) {
        _setGovernanceStrategy(governanceStrategy);
        _setVotingDelay(votingDelay);
        _guardian = guardian;

        authorizeExecutors(executors);
    }

    struct CreateVars {
        uint256 startTimestamp;
        uint256 endTimestamp;
        uint256 previousProposalsCount;
    }


/**
   * @dev Creates a Proposal (needs Voting Power of creator > Threshold)
   * @param executor - The Executor contract that will execute the proposal
   * @param targets - list of contracts called by proposal's associated transactions
   * @param values - list of value in wei for each propoposal's associated transaction
   * @param signatures - list of function signatures (can be empty) to be used when created the callData
   * @param calldatas - list of calldatas: if associated signature empty, calldata ready, else calldata is arguments
   * @param ipfsHash - IPFS hash of the proposal
   **/
    function createProposal(
        IExecutor executor,
        address[] memory targets,
        uint256[] memory values,
        string[] memory signatures,
        bytes[] memory calldatas,
        bytes32 ipfsHash
    ) external override returns (uint256) {
        require(targets.length != 0, "Governance: targets is empty");
        require(
            targets.length == values.length &&
                targets.length == signatures.length &&
                targets.length == calldatas.length,
            "Governance: Inconsistent params lenhth"
        );

        require(
            isExecutorAuthorized(address(executor)),
            "Governance: Executor is not authorized"
        );

        require(
            IProposalValidator(address(executor)).validateCreatorOfProposal(
                this,
                msg.sender
            ),
            "Governance: There is not enought voting power to create proposition"
        );

        CreateVars memory vars;

        vars.startTimestamp = block.timestamp + _votingDelay;
        vars.endTimestamp =
            vars.startTimestamp +
            IProposalValidator(address(executor)).votingDuration();

        vars.previousProposalsCount = _proposalsCount;

        Proposal storage newProposal = _proposals[vars.previousProposalsCount];

        newProposal.id = vars.previousProposalsCount;
        newProposal.creator = msg.sender;
        newProposal.executor = executor;
        newProposal.targets = targets;
        newProposal.values = values;
        newProposal.signatures = signatures;
        newProposal.calldatas = calldatas;
        newProposal.startTimestamp = vars.startTimestamp;
        newProposal.endTimestamp = vars.endTimestamp;
        newProposal.strategy = _governanceStrategy;
        newProposal.ipfsHash = ipfsHash;
        _proposalsCount++;

        emit ProposalCreated(
            vars.previousProposalsCount,
            msg.sender,
            executor,
            targets,
            values,
            signatures,
            calldatas,
            vars.startTimestamp,
            vars.endTimestamp,
            _governanceStrategy,
            ipfsHash
        );

        return newProposal.id;
    }

    /**
     * @dev Cancels a Proposal.
     * - Callable by the _guardian with relaxed conditions, or by anybody if the conditions of
     *   cancellation on the executor are fulfilled, hence is the creator will not hold the proposionThreshold on his wallet
     * @param proposalId id of the proposal
     **/
    function cancelProposal(uint256 proposalId) external override {
        ProposalState state = getProposalState(proposalId);
        require(
            state != ProposalState.Executed &&
                state != ProposalState.Canceled &&
                state != ProposalState.Expired,
            "Governance: Cancellation is not available"
        );

        Proposal storage proposal = _proposals[proposalId];
        require(
            msg.sender == _guardian ||
                IProposalValidator(address(proposal.executor))
                    .validateProposalCancellation(this, proposal.creator),
            "Governance: Cancellation is not available"
        );
        proposal.canceled = true;
        for (uint256 i = 0; i < proposal.targets.length; i++) {
            proposal.executor.cancelTransaction(
                proposal.targets[i],
                proposal.values[i],
                proposal.signatures[i],
                proposal.calldatas[i],
                proposal.executionTime
            );
        }

        emit ProposalCanceled(proposalId);
    }

    /**
     * @dev Queue the proposal (If Proposal Succeeded)
     * @param proposalId id of the proposal to queue
     **/
    function queueProposal(uint256 proposalId) external override {
        require(
            getProposalState(proposalId) == ProposalState.Succeeded,
            "Governance:Invalid state for queue"
        );
        Proposal storage proposal = _proposals[proposalId];
        uint256 executionTime = block.timestamp.add(
            proposal.executor.getDelay()
        );
        for (uint256 i = 0; i < proposal.targets.length; i++) {
            _queueOrRevert(
                proposal.executor,
                proposal.targets[i],
                proposal.values[i],
                proposal.signatures[i],
                proposal.calldatas[i],
                executionTime
            );
        }
        proposal.executionTime = executionTime;

        emit ProposalQueued(proposalId, executionTime, msg.sender);
    }

    /**
     * @dev Execute the proposal (If Proposal Queued)
     * @param proposalId id of the proposal to execute
     **/
    function executeProposal(uint256 proposalId) external payable override {
        require(
            getProposalState(proposalId) == ProposalState.Queued,
            "Governance: Allowed to execute only queued proposal"
        );
        Proposal storage proposal = _proposals[proposalId];
        proposal.executed = true;
        for (uint256 i = 0; i < proposal.targets.length; i++) {
            proposal.executor.executeTransaction{value: proposal.values[i]}(
                proposal.targets[i],
                proposal.values[i],
                proposal.signatures[i],
                proposal.calldatas[i],
                proposal.executionTime
            );
        }
        emit ProposalExecuted(proposalId, msg.sender);
    }

    /**
     * @dev Function allowing msg.sender to vote for/against a proposal
     * @param proposalId id of the proposal
     * @param support boolean, true = vote for, false = vote against
     **/
    function submitVote(uint256 proposalId, bool support) external override {
        return _submitVote(msg.sender, proposalId, support);
    }

    /**
     * @dev Set new GovernanceStrategy, allowed to call by onlyOwner
     * @notice owner should be a executor, so needs to make a proposal
     * @param governanceStrategy new Address of the GovernanceStrategy contract
     **/
    function setGovernanceStrategy(address governanceStrategy)
        external
        override
        onlyOwner
    {
        _setGovernanceStrategy(governanceStrategy);
    }

    /**
     * @dev Set new Voting Delay (delay before a newly created proposal can be voted on), allowed to call by onlyOwner
     * @notice owner should be a executor, so needs to make a proposal
     * @param votingDelay new voting delay in terms of blocks
     **/
    function setVotingDelay(uint256 votingDelay) external override onlyOwner {
        _setVotingDelay(votingDelay);
    }

    /**
     * @dev Add new addresses to the list of authorized executors, allowed to call by onlyOwner
     * @notice owner should be a executor, so needs to make a proposal
     * @param executors list of new addresses to be authorized executors
     **/
    function authorizeExecutors(address[] memory executors)
        public
        override
        onlyOwner
    {
        for (uint256 i = 0; i < executors.length; i++) {
            _authorizeExecutor(executors[i]);
        }
    }

    /**
     * @dev Remove addresses from the list of authorized executors
     * @notice owner should be a executor, so needs to make a proposal
     * @param executors list of addresses to be removed as authorized executors
     **/
    function unauthorizeExecutors(address[] memory executors)
        public
        override
        onlyOwner
    {
        for (uint256 i = 0; i < executors.length; i++) {
            _unauthorizeExecutor(executors[i]);
        }
    }

    /**
     * @dev Let the guardian abdicate from its priviledged rights. Set address of _guardian as zero address
     * @notice can be called only by _guardian
     **/
    function __abdicate() external override onlyGuardian {
        _guardian = address(0);
    }

    /**
     * @dev Getter of the current GovernanceStrategy address
     * @return The address of the current GovernanceStrategy contract
     **/
    function getGovernanceStrategy() external view override returns (address) {
        return _governanceStrategy;
    }

    /**
     * @dev Getter of the current Voting Delay (delay before a created proposal can be voted on)
     * Different from the voting duration
     * @return The voting delay in seconds
     **/
    function getVotingDelay() external view override returns (uint256) {
        return _votingDelay;
    }

    /**
     * @dev Returns whether an address is an authorized executor
     * @param executor address to evaluate as authorized executor
     * @return true - if authorized, false - is not authorized
     **/
    function isExecutorAuthorized(address executor)
        public
        view
        override
        returns (bool)
    {
        return _authorizedExecutors[executor];
    }

    /**
     * @dev Getter the address of the guardian, that can mainly cancel proposals
     * @return The address of the guardian
     **/
    function getGuardian() external view override returns (address) {
        return _guardian;
    }

    /**
     * @dev Getter of the proposal count (the current number of proposals ever created)
     * @return the proposal count
     **/
    function getProposalsCount() external view override returns (uint256) {
        return _proposalsCount;
    }

    /**
     * @dev Getter of a proposal by id
     * @param proposalId id of the proposal to get
     * @return the proposal as struct ProposalWithoutVotes
     **/
    function getProposalById(uint256 proposalId)
        external
        view
        override
        returns (ProposalWithoutVotes memory)
    {
        Proposal storage proposal = _proposals[proposalId];
        ProposalWithoutVotes
            memory proposalWithoutVotes = ProposalWithoutVotes({
                id: proposal.id,
                creator: proposal.creator,
                executor: proposal.executor,
                targets: proposal.targets,
                values: proposal.values,
                signatures: proposal.signatures,
                calldatas: proposal.calldatas,
                startTimestamp: proposal.startTimestamp,
                endTimestamp: proposal.endTimestamp,
                executionTime: proposal.executionTime,
                forVotes: proposal.forVotes,
                againstVotes: proposal.againstVotes,
                executed: proposal.executed,
                canceled: proposal.canceled,
                strategy: proposal.strategy,
                ipfsHash: proposal.ipfsHash
            });

        return proposalWithoutVotes;
    }

    /**
     * @dev Getter of the Vote of a voter about a proposal
     * @notice Vote is a struct: ({bool support, uint248 votingPower})
     * @param proposalId id of the proposal
     * @param voter address of the voter
     * @return The associated Vote memory object
     **/
    function getVoteOnProposal(uint256 proposalId, address voter)
        external
        view
        override
        returns (Vote memory)
    {
        return _proposals[proposalId].votes[voter];
    }

    /**
     * @dev Get the current state of a proposal
     * @param proposalId id of the proposal
     * @return The current state of the proposal
     **/
    function getProposalState(uint256 proposalId)
        public
        view
        override
        returns (ProposalState)
    {
        require(
            _proposalsCount > proposalId,
            "Governance: Invalid proposal ID"
        );
        Proposal storage proposal = _proposals[proposalId];
        if (proposal.canceled) {
            return ProposalState.Canceled;
        } else if (block.timestamp <= proposal.startTimestamp) {
            return ProposalState.Pending;
        } else if (block.timestamp <= proposal.endTimestamp) {
            return ProposalState.Active;
        } else if (
            !IProposalValidator(address(proposal.executor)).isProposalPassed(
                this,
                proposalId
            )
        ) {
            return ProposalState.Failed;
        } else if (proposal.executionTime == 0) {
            return ProposalState.Succeeded;
        } else if (proposal.executed) {
            return ProposalState.Executed;
        } else if (
            proposal.executor.isProposalOverExecutionPeriod(this, proposalId)
        ) {
            return ProposalState.Expired;
        } else {
            return ProposalState.Queued;
        }
    }

    function _queueOrRevert(
        IExecutor executor,
        address target,
        uint256 value,
        string memory signature,
        bytes memory callData,
        uint256 executionTime
    ) internal {
        require(
            !executor.isActionQueued(
                keccak256(
                    abi.encode(
                        target,
                        value,
                        signature,
                        callData,
                        executionTime
                    )
                )
            ),
            "Governance: Transaction is already queued"
        );
        executor.queueTransaction(
            target,
            value,
            signature,
            callData,
            executionTime
        );
    }

    function _submitVote(
        address voter,
        uint256 proposalId,
        bool support
    ) internal {
        require(
            getProposalState(proposalId) == ProposalState.Active,
            "Governance:Voting is closed"
        );
        Proposal storage proposal = _proposals[proposalId];
        Vote storage vote = proposal.votes[voter];

        require(vote.votingPower == 0, "Governance: Vote already submitted");

        uint256 votingPower = IGovernanceStrategy(proposal.strategy)
            .getVotingPower(voter);

        if (support) {
            proposal.forVotes = proposal.forVotes.add(votingPower);
        } else {
            proposal.againstVotes = proposal.againstVotes.add(votingPower);
        }

        vote.support = support;
        vote.votingPower = uint248(votingPower);

        emit VoteEmitted(proposalId, voter, support, votingPower);
    }

    function _setGovernanceStrategy(address governanceStrategy) internal {
        _governanceStrategy = governanceStrategy;

        emit GovernanceStrategyChanged(governanceStrategy, msg.sender);
    }

    function _setVotingDelay(uint256 votingDelay) internal {
        _votingDelay = votingDelay;

        emit VotingDelayChanged(votingDelay, msg.sender);
    }

    function _authorizeExecutor(address executor) internal {
        _authorizedExecutors[executor] = true;
        emit ExecutorAuthorized(executor);
    }

    function _unauthorizeExecutor(address executor) internal {
        _authorizedExecutors[executor] = false;
        emit ExecutorUnauthorized(executor);
    }
}
