// SPDX-License-Identifier:MIT
pragma solidity 0.8.9;
pragma abicoder v2;

interface IGovernanceStrategy {
    /**
     * @dev Returns the total supply of Outstanding Voting Tokens
     * @return total supply at blockNumber
     **/
    function getTotalVotingSupply() external view returns (uint256);

    /**
     * @dev Returns the Vote Power of a user at a specific block number.
     * @param user Address of the user.
     * @return Vote number
     **/
    function getVotingPower(address user) external view returns (uint256);
}
