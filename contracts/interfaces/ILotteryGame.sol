// SPDX-License-Identifier: MIT

pragma solidity 0.8.9;

interface ILotteryGame {
    function lockTransfer() external;

    function unlockTransfer() external;

    function startGame(uint256 _gasPrice, uint256 _provableGasLimit)
        external
        payable;

    function restartProvableQuery(uint256 _gasPrice, uint256 _provableGasLimit)
        external
        payable;
}
