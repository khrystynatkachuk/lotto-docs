// SPDX-License-Identifier: MIT

pragma solidity 0.8.9;

pragma abicoder v2;

interface ISwapTokens {
    function swap(uint256 amount) external;
}