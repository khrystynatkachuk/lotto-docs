// SPDX-License-Identifier: MIT

pragma solidity 0.8.9;
pragma abicoder v2;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "../interfaces/ISwapTokens.sol";


contract GameLotteryToken is Ownable, ERC20 {
    ISwapTokens public swapContract;
    
    constructor(address _swapContract) ERC20("Game Lotto", "gLOTTO")   
    {
        swapContract = ISwapTokens(_swapContract);    
    }
    modifier onlySwapContract(){
        require(msg.sender == address(swapContract), "GameLotteryToken: Caller is not the swap contract");
        _;
    }

    function mint(address account, uint256 amount) public onlySwapContract{
       _mint(account, amount);
    }

}